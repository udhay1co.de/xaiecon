#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Module for hcaptcha verifications
#

from flask_babel import Babel

babel = Babel()
